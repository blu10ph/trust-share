package cn.blu10ph.trustshare.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <p> ServiceConfig </p >
 *
 * @author cxx
 * @date 2024/4/1 20:54
 */
@Data
@Slf4j
@Component
public class ServiceConfig {

    @Value("${trustShare.pgp.secretKey}")
    private String secretKey;

    @Value("${trustShare.node.name}")
    private String name;

    @Value("${trustShare.node.email}")
    private String email;

    @Value("${trustShare.node.password}")
    private String password;

    @Value("${trustShare.dht.serverIp:0.0.0.0}")
    private String serverIp;

    @Value("${trustShare.dht.port}")
    private int port;

}
