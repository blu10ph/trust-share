package cn.blu10ph.trustshare.config;

import cn.blu10ph.trustshare.handler.DHTHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * <p> ServiceChannelInitializer </p >
 *
 * @author cxx
 * @date 2024/3/30 23:02
 */
@Component
public class ServiceChannelInitializer extends ChannelInitializer<NioDatagramChannel> {
    @Resource
    private DHTHandler dhtHandler;
    @Override
    protected void initChannel(NioDatagramChannel socketChannel) {
        socketChannel.pipeline().addLast("decoder", new StringDecoder(CharsetUtil.UTF_8));
        socketChannel.pipeline().addLast("encoder", new StringEncoder(CharsetUtil.UTF_8));
        socketChannel.pipeline().addLast(dhtHandler);
    }
}
