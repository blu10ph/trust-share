package cn.blu10ph.trustshare.constant;

/**
 * <p> Constant </p >
 *
 * @author cxx
 * @date 2024/4/1 22:56
 */
public class Constant {

    public static final long PING_TIME = 60 * 1000L; // 30 * TODO DEBUG

    public static final long OUT_TIME = PING_TIME * 2;

    public static final String TRUST_LEVEL_ULTIMATELY = "ultimately";
    public static final String TRUST_LEVEL_FULLY = "fully";
    public static final String TRUST_LEVEL_MARGINALLY = "marginally";
    public static final String TRUST_LEVEL_NOT = "not";

    public static final String MSG_TYPE_GET_PUBLIC_KEY = "get_public_key";
    public static final String MSG_TYPE_PING = "ping";
    public static final String MSG_TYPE_FIND_NODE = "find_node";
    public static final String MSG_TYPE_GET_NOTICES = "get_notices";
    public static final String MSG_TYPE_ANNOUNCE_NOTICE = "announce_notice";

    public static final String MSG_DIRECTION_QUERY = "query";
    public static final String MSG_DIRECTION_RETURN = "return";

}
