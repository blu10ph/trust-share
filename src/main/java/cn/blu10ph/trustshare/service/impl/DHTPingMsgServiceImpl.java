package cn.blu10ph.trustshare.service.impl;

import cn.blu10ph.trustshare.bean.msg.BaseMsg;
import cn.blu10ph.trustshare.bean.msg.TrustMsg;
import cn.blu10ph.trustshare.bean.node.TrustNode;
import cn.blu10ph.trustshare.constant.Constant;
import cn.blu10ph.trustshare.service.DHTMsgService;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * <p> DHTPingMsgServiceImpl </p >
 *
 * @author cxx
 * @date 2024/4/6 22:22
 */
@Slf4j
@Component
public class DHTPingMsgServiceImpl extends DHTMsgBaseService<Object> implements DHTMsgService<Object> {

    @Override
    public void sendReturn(ChannelHandlerContext ctx, DatagramPacket packet, TrustNode node, TrustMsg msg) {
        BaseMsg msgObj = new BaseMsg();
        msgObj.setType(Constant.MSG_TYPE_PING);
        msgObj.setDirection(Constant.MSG_DIRECTION_RETURN);
        msgObj.setFrom(routingTable.getNodeId());
        msgObj.setTo(node.getNodeId());
        response(node, ctx, packet.sender(), msgObj);
    }

    @Override
    public void receive(ChannelHandlerContext ctx, DatagramPacket packet, TrustNode node, TrustMsg msg) {
        // receivePing
    }

    @Override
    protected Class<Object> getParamsType() {
        return Object.class;
    }

    @Override
    public String getType() {
        return Constant.MSG_TYPE_PING;
    }

}
