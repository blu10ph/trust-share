package cn.blu10ph.trustshare.service;

import cn.blu10ph.trustshare.bean.msg.TrustMsg;
import cn.blu10ph.trustshare.bean.node.TrustNode;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;

/**
 * <p> DHTMsgService </p >
 *
 * @author cxx
 * @date 2024/4/6 22:23
 */
public interface DHTMsgService<T> {
    void sendQuery(TrustNode node, T params) throws Exception;
    void sendReturn(ChannelHandlerContext ctx, DatagramPacket packet, TrustNode node, TrustMsg msg);
    void receive(ChannelHandlerContext ctx, DatagramPacket packet, TrustNode node, TrustMsg msg);
    T getParams(String params) throws JsonProcessingException;
    String getType();
}
