package cn.blu10ph.trustshare.bean.msg;

import cn.blu10ph.trustshare.bean.node.TrustNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

/**
 * <p> NodesMsg </p >
 *
 * @author cxx
 * @date 2024/4/1 23:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class NodesMsg extends FindNodeMsg {
    public NodesMsg(FindNodeMsg findNodeMsg){
        super(findNodeMsg.getTarget());
    }
    List<TrustNode> nodes = Collections.emptyList();
}
