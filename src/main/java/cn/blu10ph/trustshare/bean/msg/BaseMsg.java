package cn.blu10ph.trustshare.bean.msg;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p> BaseMsg </p >
 *
 * @author cxx
 * @date 2024/4/1 23:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseMsg {
    String type;
    String direction;
    String from;
    String to;
    String params;
}
