package cn.blu10ph.trustshare.bean.msg;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p> TrustMsg </p >
 *
 * @author cxx
 * @date 2024/4/1 23:40
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TrustMsg extends BaseMsg {
    public TrustMsg(BaseMsg baseMsg){
        super(baseMsg.type, baseMsg.direction, baseMsg.from, baseMsg.to, baseMsg.params);
    }
    Long time;
    String nonce;
    String sign;
}
