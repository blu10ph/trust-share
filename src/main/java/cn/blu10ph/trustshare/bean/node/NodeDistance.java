package cn.blu10ph.trustshare.bean.node;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p> NodeDistance </p >
 *
 * @author cxx
 * @date 2024/4/1 23:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NodeDistance {
    String nodeId;
    long distance;
}
