package cn.blu10ph.trustshare.bean.node;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p> PublicKeyMsg </p >
 *
 * @author cxx
 * @date 2024/4/1 23:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PublicKeyMsg {
    String target;
    TrustNode node;
    String publicKey;

    public PublicKeyMsg(GetPublicKeyMsg getPublicKeyMsg) {
        this.target = getPublicKeyMsg.getTarget();
    }
}
