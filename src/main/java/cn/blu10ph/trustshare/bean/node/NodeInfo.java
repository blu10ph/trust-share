package cn.blu10ph.trustshare.bean.node;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p> NodeInfo </p >
 *
 * @author cxx
 * @date 2024/4/1 23:40
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class NodeInfo extends BaseNode {
    public NodeInfo(BaseNode node) {
        super(node.getNodeId(), node.getHost(), node.getPort());
    }
    public NodeInfo(String nodeId, String host, int port){
        this.nodeId = nodeId;
        this.host = host;
        this.port = port;
        this.base = null;
    }
    BaseNode base;
}
