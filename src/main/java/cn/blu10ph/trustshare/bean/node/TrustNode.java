package cn.blu10ph.trustshare.bean.node;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p> TrustNode </p >
 *
 * @author cxx
 * @date 2024/4/1 23:40
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TrustNode extends NodeInfo {
    public TrustNode(BaseNode node) {
        super(node);
    }
    public TrustNode(NodeInfo node) {
        super(node.getNodeId(), node.getHost(), node.getPort());
    }
    public TrustNode(String nodeId, String host, int port) {
        super(nodeId, host, port);
    }
    Long time;
    String nonce;
    String sign;
    String trustLevel;
    Long activeTime;
}
