package cn.blu10ph.trustshare.bean.notice;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p> Notice </p >
 *
 * @author cxx
 * @date 2024/4/9 20:31
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TrustNotice extends Notice {
    public TrustNotice(Notice notice){
        super(notice.getFromNode(), notice.getBaseHash(), notice.getType(),
                notice.getContent(), notice.getTime(), notice.getInfoHash(),
                notice.getBaseReceive(), notice.getRootReceive()
        );
    }
    Long time;
    String nonce;
    String sign;
}
