package cn.blu10ph.trustshare.bean.notice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p> GetNoticesMsg </p >
 *
 * @author cxx
 * @date 2024/4/1 23:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetNoticesMsg {
    String baseHash;
    String infoHash;
    String type;
    String fromNode;
}
