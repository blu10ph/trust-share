package cn.blu10ph.trustshare.bean.notice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p> AnnounceNoticeMsg </p >
 *
 * @author cxx
 * @date 2024/4/9 20:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnnounceNoticeMsg {
    String infoHash;
}
