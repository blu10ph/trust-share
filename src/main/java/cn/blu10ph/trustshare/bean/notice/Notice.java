package cn.blu10ph.trustshare.bean.notice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p> Notice </p >
 *
 * @author cxx
 * @date 2024/4/9 20:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Notice {
    String fromNode;
    String baseHash;
    String type;
    String content;
    Long time;
    String infoHash;
    Boolean baseReceive = false;
    Boolean rootReceive = false;
}
