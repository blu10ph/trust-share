package cn.blu10ph.trustshare.bean.notice;

import cn.blu10ph.trustshare.bean.notice.GetNoticesMsg;
import cn.blu10ph.trustshare.bean.notice.TrustNotice;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

/**
 * <p> NoticesMsg </p >
 *
 * @author cxx
 * @date 2024/4/1 23:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class NoticesMsg extends GetNoticesMsg {
    public NoticesMsg(GetNoticesMsg getNoticesMsg){
        super(getNoticesMsg.getBaseHash(), getNoticesMsg.getInfoHash(), getNoticesMsg.getType(), getNoticesMsg.getFromNode());
    }
    List<TrustNotice> notices = Collections.emptyList();
}
