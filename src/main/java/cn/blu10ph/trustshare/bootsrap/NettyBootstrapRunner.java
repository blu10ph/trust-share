package cn.blu10ph.trustshare.bootsrap;

import cn.blu10ph.trustshare.service.DHTStartService;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * <p> NettyBootstrapRunner </p >
 *
 * @author cxx
 * @date 2024/3/30 22:56
 */
@Slf4j
@Component
public class NettyBootstrapRunner implements ApplicationRunner {

    @Resource
    DHTStartService service;

    @Override
    public void run(ApplicationArguments args) {
        log.info("service run!");
        service.init(args);
        log.info("service end!");
    }

}
