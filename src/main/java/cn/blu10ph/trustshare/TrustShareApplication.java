package cn.blu10ph.trustshare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class TrustShareApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrustShareApplication.class, args);
    }

}
