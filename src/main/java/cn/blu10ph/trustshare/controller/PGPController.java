package cn.blu10ph.trustshare.controller;

import cn.blu10ph.trustshare.core.PGPManager;
import cn.blu10ph.trustshare.util.PgpUtil;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p> PGPController </p >
 *
 * @author cxx
 * @date 2024/4/10 23:24
 */
@Slf4j
@RestController
@RequestMapping("/pgp")
public class PGPController {

    @Resource
    PGPManager pgpManager;

    @GetMapping(value = "/addPublicKey")
    public void addPublicKey(String publicKey) throws Exception {
        log.info("Rest add public key:{}", publicKey);
        PGPPublicKey publicKeyPbj = PgpUtil.loadPublicKey(publicKey);
        String publicKeyFingerprint = PgpUtil.getPublicKeyFingerprint(publicKeyPbj);
        pgpManager.savePublicKey(publicKeyFingerprint, publicKeyPbj);
    }

    @GetMapping(value = "/removePublicKey")
    public void removePublicKey(String id) {
        log.info("Rest remove public key:{}", id);
        pgpManager.removePublicKey(id);
    }

    @GetMapping(value = "/setTrustLevel")
    public void setTrustLevel(String id, String level) {
        log.info("Rest set trust level:{},{}", id, level);
        pgpManager.setTrustLevel(id, level);
    }

    @GetMapping(value = "/removeTrustLevel")
    public void removeTrustLevel(String id) {
        log.info("Rest remove trust level:{}", id);
        pgpManager.removeTrustLevel(id);
    }

}
